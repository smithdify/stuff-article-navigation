var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    babel = require("gulp-babel"),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    rename = require("gulp-rename"),
    data_uri = require('gulp-data-uri'),
    install = require("gulp-install"),
    combiner = require('stream-combiner2'),
    del = require('del'),
    karma = require('karma'),
    Q = require('q'),
    package = require('./package.json'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

var appFilename = package.name + '-' + package.version;

gulp.task('build:deps', function () {
    console.log('build:deps');
    gulp.src(['./package.json'])
        .pipe(install());
});

gulp.task('clean:styles', function () {
    return del([
        'dist/css/**/*'
    ]);
});

gulp.task('clean:scripts', function () {
    return del([
        'dist/js/**/*'
    ]);
});

gulp.task('styles', ['clean:styles'], function () {
    console.log('executing::styles');
    return gulp.src('src/scss/next-article.scss')
        .pipe(sass({outputStyle: 'compressed', noCache: true, errLogToConsole: true, sourcemap: false}))
        .on("error", function (error) {
            console.error(error);
            this.emit('end');
        })
        .pipe(autoprefixer({
            browsers: ['last 2 versions', '> 1%'], // These are default values
            cascade: false
        }))
        .pipe(data_uri())
        .pipe(rename(appFilename + '.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(reload({stream: true}));
});

gulp.task('scripts', ['clean:scripts'], function () {
    console.log('executing::scripts');
    var combined = combiner.obj([
        gulp.src('src/js/**/*.js'),
        babel(),
        //uglify(),
        concat(appFilename + '.js'),
        gulp.dest('dist/js'),
        reload({stream: true})
    ]);
    combined.on('error', function (error) {
        console.error(error);
        this.emit('end');
    });
    return combined;
});

gulp.task('test', function (done) {
    var deferred = Q.defer();

    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, function (exitCode) {
        if (!exitCode) {
            // If we don't resolve (because a test failed), then
            // subsequent tasks won't run.
            deferred.resolve();
        }
    }).start();

    return deferred.promise;
});

gulp.task('buildApp', ['styles', 'scripts'], function () {
    gulp.src('dist/js/next-article*.js')
        .pipe(rename(package.name + '.js'))
        .pipe(gulp.dest('app/js'));
    gulp.src('dist/css/next-article*.css')
        .pipe(rename(package.name + '.css'))
        .pipe(gulp.dest('app/css'));

});

// watch Sass files for changes, run the Sass preprocessor with the 'sass' task and reload
gulp.task('serve', ['buildApp'], function () {
    browserSync({
        server: {
            baseDir: 'app',
            index: 'test.html'
        }
    });

    //gulp.watch('src/scss/*.scss', ['buildApp']);
    //gulp.watch('src/js/**/*', ['buildApp']);
});

// Watch Files For Changes
gulp.task('watch', ['serve', 'scripts', 'styles'], function () {
    gulp.watch('src/js/**/*', ['buildApp', reload]);
    gulp.watch('src/scss/**/*', ['buildApp', reload]);
    gulp.watch('app/*.html', reload);
});

gulp.task('default', ['build:deps', 'scripts', 'styles']);
