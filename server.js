/**
 * Created by nathan.smith on 10/02/2016.
 */

const express = require("express");
const app = express();
const http = require('http');
const serveStatic = require('serve-static');
const version = require('version-healthcheck');


/*expressjs variables */
const port = 3006;

app.listen(port, function() {
    console.log('Server running on localhost:' + port);
});

app.use(serveStatic(__dirname));
app.get('/version', version);

var mainPageHandler = function(req, res) {
    /* url to proxy */
    var options = {
        //hostname: 'localhost',
        //port: 8080,
        hostname: 'www.stuff.co.nz',
        path: req.originalUrl,
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    var proxyRequest = http.request(options, function(response) {
        var body = '';
        //var callbackFn = req.params('callback');
        if (String(response.headers['content-type']).indexOf('application/json') !== -1) {
            response.on('data', function(chunk) {
                body += chunk;
            });
            response.on('end', function() {
                // Make changes to HTML files when they're done being read.
                //body = callbackFn + '(' + body + ');';

                response.headers['content-length'] = body.length;
                //res.writeHead(response.statusCode, response.headers);
                res.jsonp(body);
            });
        } else {
            response.pipe(res, {
                end: true
            });
        }
    });

    proxyRequest.end();
};

app.get('/national', mainPageHandler);
app.get('/entertainment', mainPageHandler);
app.get('/*', mainPageHandler);


var simpleProxy = function(clientRequest, clientResponse) {
    /* url to proxy */
    var options = {
        hostname: 'www.stuff.co.nz',
        path: '/' + clientRequest.url,
        method: 'GET'
    };
    var proxyRequest = http.request(options, function(response) {
        var body = '';
        if (String(response.headers['content-type']).indexOf('text/html') !== -1) {
            response.on('data', function(chunk) {
                body += chunk;
            });

            response.on('end', function() {
                clientResponse.writeHead(response.statusCode, response.headers);
                clientResponse.end(body);
            });
        } else {
            response.pipe(clientResponse, {
                end: true
            });
        }
    });

    proxyRequest.end();
};

app.get('/etc/*', simpleProxy);
app.get('/content/*', simpleProxy);
