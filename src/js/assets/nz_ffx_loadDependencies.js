/*
 ** Load our main dependencies once the polyfills have loaded.
 */
;
'use strict';

(function(namespace) {

    // Here we are loading our polyfills asynchronously,
    // followed by the main dependencies script.
    Promise.all(namespace.polyfills.promises).then(function(result){
        if (namespace.mustard) {
            // Any other scripts on the page can check existence of either
            // namespace.app or namespace.mustard to ensure mustard has been cut,
            // and then be loaded in namespace.app.then which will ensure
            // that all dependencies have been loaded.
            namespace.app = namespace.loadScript(namespace.path + '/app.min.js', false).then(
                function(result){
                    namespace.dependencies.main.resolve();
                    if (/debugClientLibs=true/.test(location.search)) {
                        if (document.querySelectorAll('.lt-ie9').length) {
                            console.log('Mustard cut and all dependencies available');
                        } else {
                            console.log('Mustard cut and all dependencies available at ' + parseInt(performance.now()) + ' milliseconds');
                        }
                    }
                }
            );
        } else {
            namespace.dependencies.main.resolve();
            if (/debugClientLibs=true/.test(location.search)) {
                console.log('Mustard not cut and all dependencies available');
            }
        }
    });

})(nz.ffx);
