/*
 ** Create our namespace, define our script loader, and externalise our
 ** main dependencies promise resolve function.
 ** This script should be the first of our scripts to appear on the page, as
 ** it defines the namespace.
 */
;
'use strict';

var nz = nz || {};
nz.ffx = nz.ffx || {};
nz.ffx.name = 'nz_ffx';
// The path to our scripts directory
nz.ffx.path = nz.ffx.path || 'js/adaptive';

(function(namespace) {

    // Set up our promise for the main dependencies bundle,
    // and externalise the resolve function to the namespace so that
    // a later script can resolve it.
    namespace.dependencies      = namespace.dependencies || {};
    namespace.dependencies.main = namespace.dependencies.main || {};

    namespace.dependencies.main.promise = new Promise(function(resolve, reject) {
        namespace.dependencies.main.resolve = resolve;
    });

    // wait for DOM content to be available so that JS on the page can find any rendered HTML element. Also waits
    // for the main dependencies to be loaded.
    namespace.dependencies.domLoaded = namespace.dependencies.domLoaded || {};
    namespace.dependencies.domLoaded.promise = new Promise(function(resolve) {
        addEventListener("DOMContentLoaded", function() {
            namespace.dependencies.main.promise.then(function() {
                resolve();
                if (/debugClientLibs=true/.test(location.search)) {
                    if (document.querySelectorAll('.lt-ie9').length) {
                        console.log('nz.ffx.dependencies.domLoaded.promise resolved');
                    } else {
                        console.log('nz.ffx.dependencies.domLoaded.promise resolved at ' + parseInt(performance.now()) + ' milliseconds');
                    }
                }
            });
        });
    });

    // Generating promise to ensure we wait for DOM elements to be moved adaptively
    // Example: waiting for mrec Ad to be relocated before loading its Ad code
    namespace.dependencies.adaptTo = namespace.dependencies.adaptTo || {};
    namespace.dependencies.adaptTo.promise = new Promise(function(resolve, reject) {
        namespace.dependencies.adaptTo.resolve = resolve;
    });

    // Setup a promise to wait for the availability of Adobe's window.s variable.
    // Later, we can resolve this promise when we get the event from DTM.
    namespace.dependencies.siteCatalyst = namespace.dependencies.siteCatalyst || {};
    namespace.dependencies.siteCatalyst.promise = new Promise(function(resolve, reject) {
        namespace.dependencies.siteCatalyst.resolve = resolve;
    });

    namespace.loadScript = function (url, async, success, failure) {
        var scriptPromise = new Promise(function(resolve, reject) {
            // Create a new script tag
            var script = document.createElement('script');

            // Use the url argument as source attribute
            script.src = url;

            // Scripts loaded synchronously will execute in series,
            // which is what we want at times
            script.async = async;

            // Call resolve when it’s loaded
            if (script.addEventListener) {
                script.addEventListener('load', function() {
                    resolve(url);
                }, false);
            } else {
                script.attachEvent('load', function() {
                    resolve(url);
                });
            }

            // Reject the promise if there’s an error
            if (script.addEventListener) {
                script.addEventListener('error', function() {
                    reject(url);
                }, false);
            } else {
                script.attachEvent('error', function() {
                    reject(url);
                });
            }

            if (/debugClientLibs=true/.test(location.search)) {
                if (document.querySelectorAll('.lt-ie9').length) {
                    console.log('Loading script ' + url);
                } else {
                    console.log('Loading script ' + url + ' at ' + parseInt(performance.now()) + ' milliseconds');
                }
            }

            var head = window.document.getElementsByTagName('head')[0];
            head.appendChild(script);
        });

        return scriptPromise;
    }

})(nz.ffx)
