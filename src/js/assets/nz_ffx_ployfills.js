/*
 ** Check whether we cut the mustard, and load our polyfills if we do.
 */
;
'use strict';

(function(namespace) {

    // simulate lack of browser API support
    //window.matchMedia = false;
    //window.HTMLPictureElement = false;
    namespace.polyfills = namespace.polyfills || {};
    namespace.polyfills.promises = [];

    // Do we cut the mustard?
    if('querySelector' in document
        && 'localStorage' in window
        && 'addEventListener' in window) {

        namespace.mustard = true;

        // The browser cuts the mustard, so we're prepared to
        // polyfill where necessary

        // Do we have support for media queries?
        if (!window.matchMedia) {
            var polyfillMatchMedia = new Promise(function(resolve, reject) {
                resolve(namespace.loadScript(namespace.path + '/matchMedia.js', false))
            });
            namespace.polyfills.promises.push(polyfillMatchMedia);
        }

        // Do we have support for the picture element?
        if (!window.HTMLPictureElement) {
            var polyfillPicture = new Promise(function(resolve, reject) {
                resolve(namespace.loadScript(namespace.path + '/respimage.js', false))
            });
            namespace.polyfills.promises.push(polyfillPicture);
        }
    } else {
        namespace.mustard = false;

        var jqueryLegacy = new Promise(function(resolve, reject) {
            resolve(namespace.loadScript(namespace.path + '/jquery.min.legacy.js', false))
        });
        namespace.polyfills.promises.push(jqueryLegacy);
    }

})(nz.ffx)
