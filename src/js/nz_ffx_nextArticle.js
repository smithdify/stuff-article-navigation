/** @namespace */
var nz = nz || {};
nz.ffx = nz.ffx || {};
nz.ffx.config = nz.ffx.config || {};

(function (namespace) {
    'use strict';

    var defaults = {
        container: '.story',
        default: {
            prevText: 'Previous Article title',
            nextText: "Next Article title"
        },
        // TODO: Use ${digitalData.page.category.primaryCategory} if available in artile feed request
        contentUrl: `/_json/?mostpopular=20&limit=20`
    };

    const onRejected = function (reason) {
        console.error(reason);
    };

    var NextArticle = function (options) {
        var settings = $.extend({}, defaults, options);

        function getTemplate() {
            return `<div id="NextPrevPosts">` +
                `<div class="prev">` +
                `<div class="arrow"><svg version="1.1" id="PrevArrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 11.289 27.701" enable-background="new 0 0 11.289 27.701" xml:space="preserve"><polyline fill="none" stroke="#232323" opacity="0.4" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" points="9.501,26.201 1.499,13.851 9.501,1.5 "></polyline></svg></div>` +
                `<div class="preview">` +
                `<div class="pull-left featuredImg"></div>` +
                `<div class="pull-left preview-content">` +
                `<h1>${settings.default.prevText}</h1>` +
                `</div>` +
                `</div>` +
                `</div>` +
                `<div class="next">` +
                `<div class="arrow"><svg version="1.1" id="NextArrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 11.289 27.701" enable-background="new 0 0 11.289 27.701" xml:space="preserve"><polyline fill="none" stroke="#232323" opacity="0.4" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" points="1.5,1.5 9.502,13.85 1.5,26.201 "></polyline></svg></div>` +
                `<div class="preview">` +
                `<div class="pull-right featuredImg"></div>` +
                `<div class="pull-right preview-content">` +
                `<h1>${settings.default.nextText}</h1>` +
                `</div>` +
                `</div>` +
                `</div>` +
                `</div>`;
        }

        function postFeedServiceInit(articles) {

            $(document).ready(function () {
                console.log('NextArticle.init');

                var $container = $(settings.container);
                var $template = $(getTemplate());
                $container.append($template);
                var navigateArticles = new nz.ffx.NavigateArticles({
                    articles: articles,
                    $container: $container
                });
                navigateArticles.init();
            });
        }

        function init() {
            new nz.ffx.ArticleFeedService({
                contentUrl: settings.contentUrl
            }).init()
                .then(postFeedServiceInit, onRejected)
                .catch(onRejected);
        }

        return {
            init: init
        };
    };

    namespace.NextArticle = NextArticle;
})(nz.ffx);
