/** @namespace */
var nz = nz || {};
nz.ffx = nz.ffx || {};
nz.ffx.config = nz.ffx.config || {};

(function (namespace) {
    'use strict';

    var defaults = {
        hostUrl: 'http://localhost:3006'
    };

    // TODO Look at storing articles in localstorage - Lance
    var ArticleFeedService = function (options) {
        var settings = $.extend({}, defaults, options);

        function initArticles() {
            return new Promise(function (resolve, reject) {
                var articles = [];
                $.ajax({
                    url: encodeURI(settings.hostUrl + settings.contentUrl),
                    async: false,
                    type: "GET",
                    contentType: 'application/json',
                    cache: false,
                    dataType: 'jsonp',
                    crossDomain: true
                }).done(function (data, textStatus, xhr) {
                    var articlesJson = JSON.parse(data);
                    if (articlesJson.stories) {
                        for (var i = 0; i < articlesJson.stories.length; i++) {

                            var story = articlesJson.stories[i];
                            var url = story.url.replace(/\/_json\//, '/');

                            var article = {
                                id: story.id,
                                datetime: story.datetime_display,
                                headline: story.alt_headline || story.title,
                                url: url,
                                intro: story.alt_intro,
                                body: story.body
                            };

                            if (story.source_code === 'sponsored') {
                                article.sponsored = true;
                            }

                            if (story.images && story.images.length > 0) {
                                //find first strap image as a higher res image than thumb
                                var strapImage = null;
                                var standardImage = null;
                                for (var j = 0; j < story.images.length; j++) {
                                    var image = story.images[j];
                                    if (image.variants && image.variants.length > 0) {
                                        for (var k = 0; k < image.variants.length; k++) {
                                            var variant = image.variants[k];
                                            if (variant.layout === 'Strap Image') {
                                                strapImage = {};
                                                strapImage.caption = image.caption;
                                                strapImage.creditline = image.creditline;
                                                strapImage.source = variant.src;
                                                break;
                                            }
                                            if (variant.layout === 'Standard Image') {
                                                standardImage = {};
                                                standardImage.caption = image.caption;
                                                standardImage.creditline = image.creditline;
                                                standardImage.source = variant.src;
                                                break;
                                            }
                                        }
                                    }
                                    if (strapImage !== null && standardImage !== null) {
                                        break;
                                    }
                                }

                                if (strapImage !== null && standardImage) {
                                    article.previewImage = strapImage;
                                    article.storyImage = standardImage;
                                } else {
                                    var imageItem = story.images[0]; //first image is thumbnmail
                                    article.previewImage = {};
                                    article.previewImage.caption = imageItem.caption;

                                    if (imageItem.variants && imageItem.variants.length > 0) {
                                        article.previewImage.source = imageItem.variants[0].src;
                                    }
                                }
                            }
                            articles.push(article);
                        }
                    }
                    resolve(articles);
                }).fail(function (xhr, status, error) {
                    var msg = "Could not request articles from Stuff JSON feed.\nStatus: " + status + "\nMessage: " + error;
                    reject(msg);
                });
            });
        }

        function init() {
            return initArticles();
        }

        return {
            init: init
        };
    };

    namespace.ArticleFeedService = ArticleFeedService;
})(nz.ffx);
