/** @namespace */
var nz = nz || {};
nz.ffx = nz.ffx || {};
nz.ffx.config = nz.ffx.config || {};

(function (namespace) {
    'use strict';

    var defaults = {
        articles: [],
        $container: undefined,
        prevSelector: '.prev',
        nextSelector: '.next',
        nextImageSelector: '.pull-right.featuredImg',
        nextTextSelector: '.pull-right.preview-content h1',
        prevImageSelector: '.pull-left.featuredImg',
        prevTextSelector: '.pull-left.preview-content h1'
    };

    var NavigateArticles = function (options) {
        var settings = $.extend({}, defaults, options);
        var currentIndex = 0;
        var prevHandle;
        var nextHandle;

        function init() {
            prevHandle = $(settings.prevSelector);
            nextHandle = $(settings.nextSelector);
            // When this function is called, articles and the $container will have values
            displayArticle(settings.articles[currentIndex]);
            updateNextPreview();
            updatePrevPreview();
            nextHandle.on('click', function (e) {
                e.preventDefault();
                // Display the next article
                displayNextArticle();
            });
            prevHandle.on('click', function (e) {
                e.preventDefault();
                // Display the previous article
                displayPrevArticle();
            });
        }

        function haveNextArticle() {
            return !(currentIndex + 1 > settings.articles.length);
        }

        function havePreviousArticle() {
            return !(currentIndex == 0 || currentIndex - 1 < 0);
        }

        function gotoNextArticle() {
            if (haveNextArticle()) {
                currentIndex++;
                return true;
            }
            return false;
        }

        function gotoPrevArticle() {
            if (havePreviousArticle()) {
                currentIndex--;
                return true;
            }
            return false;
        }

        function updateNextPreview() {
            // Next article won't have a value the first time in
            // Wrap around to the beginning
            if (!haveNextArticle()) {
                currentIndex = 0;
            }
            var nextArticle = settings.articles[currentIndex + 1];
            nextHandle.find(settings.nextImageSelector).css("background-image", 'url(' + nextArticle.previewImage.source + ')');
            nextHandle.find(settings.nextTextSelector).text(nextArticle.headline);

        }

        function updatePrevPreview() {
            var prevArticle;
            // No previous article, provide a blank article to clear out previous preview.
            if (!havePreviousArticle()) {
                prevHandle.addClass('is-disabled');
                prevArticle = {
                    previewImage: {
                        source: ''
                    },
                    headline: ''
                };
            } else {
                prevHandle.removeClass('is-disabled');
                prevArticle = settings.articles[currentIndex - 1];
            }
            prevHandle.find(settings.prevImageSelector).css("background-image", 'url(' + prevArticle.previewImage.source + ')');
            prevHandle.find(settings.prevTextSelector).text(prevArticle.headline);

        }

        function displayNextArticle() {
            if (gotoNextArticle()) {
                displayArticle(settings.articles[currentIndex]);
                updateNextPreview();
                updatePrevPreview();
            }
        }

        function displayPrevArticle() {
            if (gotoPrevArticle()) {
                displayArticle(settings.articles[currentIndex]);
                updateNextPreview();
                updatePrevPreview();
            }
        }

        function displayArticle(article) {
            $('.story__headline__text').text(article.intro);
            const $dateTime = $('.story__byline-date');
            $dateTime.text(article.datetime);
            $dateTime.attr('content', article.datetime);
            const $articleImage = $('div.media-item__asset img');
            $articleImage.attr('src', article.storyImage.source);
            $articleImage.attr('srcset',
                article.previewImage.source + ' 320w, '
                + article.storyImage.source + ' 620w');
            $('span.media-item__photographer').text(article.storyImage.creditline);
            $('div.media-item__caption span p').text(article.storyImage.caption);
            $('div.story__content > p').remove();
            $('div.story__content > div').remove();
            $('div.story__content figure').after(article.body);
        }

        return {
            init: init,
            getNextArticle: gotoNextArticle,
            getPrevArticle: gotoPrevArticle
        };
    };

    namespace.NavigateArticles = NavigateArticles;
})(nz.ffx);
